import sqlite3 as lite
import urllib2
import re
import cPickle
from BeautifulSoup import BeautifulSoup

_brackets = re.compile(r"\(\S+\s*\S*\s*\S*\s*\S*\)")
con = lite.connect("food.db")


def fill_ingredients():
	master_rcps = getall_rcp_tups()
	for rcp in master_rcps:
		pagesoup = get_pagesoup(rcp[1])
		all_ingrs = getall_ingrs_from_page(pagesoup)
		for ingredient in all_ingrs:
			add_ingr(ingredient, rcp[0])
def getall_rcp_tups():
	"""Return list of (rcp Id, rcp url) tuples from database"""
	cur = con.cursor()
	tups = cur.execute("SELECT Id, url FROM Recipe").fetchall()
	return tups

def get_pagesoup(pageurl):
	page = urllib2.urlopen(urllib2.Request(pageurl)).read()
	soup = BeautifulSoup(page)
	return soup

def getall_ingrs_from_page(soup):
	"""Return list of ingredients from a page"""
	ingr_table = soup.findAll("tr", attrs={"align":"center"})
	ingrs = []
	for elem in ingr_table:
		if len(elem) > 1:
			ingrs.append(clean_ingr(elem.contents[3].string))
	ingrs = filter(None, ingrs)
	try:
		ingrs.remove('&nbsp;')
		return ingrs
	except:
		return ingrs

def clean_ingr(ingr):
	if ingr:
		brackets = re.findall(_brackets, str(ingr))
		if brackets:
			ingr = ingr.strip(brackets[0]).strip()
			print ingr
		return ingr.lower()

def add_ingr(ingr, rcp_id):
	#check if in db
	cur = con.cursor()
	ingr_rcps = cur.execute("SELECT recipes FROM Ingredient WHERE name=?", (ingr,)).fetchone()
	print rcp_id, ingr_rcps
	if ingr_rcps is None:
		new_rcp = cPickle.dumps([rcp_id])
		cur.execute("INSERT INTO Ingredient(name, recipes) VALUES (?,?)", (ingr,new_rcp))
		con.commit()
	else:
		existing_rcps = cPickle.loads(str(ingr_rcps[0]))
		existing_rcps.append(rcp_id)
		existing_rcps = cPickle.dumps(existing_rcps)
		cur.execute("UPDATE Ingredient SET recipes=? WHERE name=?", (existing_rcps,ingr))
		con.commit()




	#if not, add [rcp]
	#if yes, get thing back and append yo


fill_ingredients()
