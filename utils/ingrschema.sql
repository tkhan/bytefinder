DROP TABLE IF EXISTS Ingredients;
CREATE TABLE Ingredients (
	Id integer PRIMARY KEY AUTOINCREMENT,
	name string not null,
	recipes string,
);
