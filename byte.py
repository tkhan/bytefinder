import sqlite3
import json
import ast
import cPickle
from collections import Counter
from excerpt_extractor import get_summary
from flask import Flask, request, session, g, redirect, url_for, \
     abort, render_template, flash

# configuration
DATABASE = 'C:\\Users\\Buster\\Heroku\\bytefinder\\food.db'
DEBUG = True

app = Flask(__name__)
app.config.from_object(__name__)

def connect_db():
    return sqlite3.connect(app.config['DATABASE'])

@app.before_request
def before_request():
    g.db = connect_db()

@app.teardown_request
def teardown_request(exception):
    g.db.close()

@app.route('/jsontimes/')
def jsontimes():
    q = request.args.get('term')
    cur =  g.db.execute("SELECT * FROM Ingredient WHERE name LIKE ?", ('%'+q+'%',))
    entries = cur.fetchall()
    search = []
    for entry in entries:
	    search.append({'id':entry[0], 'value':entry[1], 'label':entry[1]})

    return json.dumps(search)

@app.route('/', methods=['GET', 'POST'])
def index():
	results, error, match = None, None, None
	if request.method == 'POST':
		search_str = request.form['search']
		results, error, match = compile_rcps(search_str)
	return render_template('search_front.html', error=error, results= results, match=match)


#################
### UTILITIES ###
#################

def compile_rcps(search_str):
	""" Given search string, returns a list of combined recipe ids from all ingredients searched"""
	results, error, match = None, None, None
	search = [n.strip() for n in search_str.split(',')]
	search = filter(None, search)
	main = []
	for ingredient in search:
		cur = g.db.execute("SELECT recipes FROM Ingredient WHERE name=?", (ingredient,)).fetchone()
		if cur is None:
			error = "whoops - we can't find '%s' in the ingredient list! please try another search" % ingredient
			return (results, error, match)
		rcps = cur[0]
		#rcps = ast.literal_eval(rcps)
		rcps = cPickle.loads(str(rcps))
		main.extend(rcps)
	try:
		sorted_rcps = ordering(search, main)
		url_list, max_match = get_urls(len(search), sorted_rcps)
		results = get_blurbs(url_list)
		match = qualify_match(max_match)
	except:
			error = "whoops - something went wrong! please try another search"

	return (results, error, match)

def ordering(search_l, compiled_rcps):
	""" Returns dictionary of {number of ingredients matched:[recipe1, recipe2...]}"""
	repeats = {}
	i, count = 1, 0
	maxlen = len(search_l)
	while i <= maxlen:	# init empty dict
		repeats[i]=[]
		i += 1
	
	ordered = Counter(compiled_rcps)
	ordered = ordered.items()
	for i in ordered:
		if i[1] > maxlen: 
			repeats[maxlen].append(i[0])
		else:
			repeats[i[1]].append(i[0])
	
	return repeats

def get_urls(max_len, ordered):
	i = max_len
	max_match = None
	rcps = []
	while i and len(rcps) < 10:
		if len(ordered[i])>0 and not max_match:
			max_match = float(i/max_len) * 100
		rcps.extend(ordered[i])
		i -= 1
	
	rcps = rcps[:10]
	urls = []
	for rcp in rcps:
		cur = g.db.execute("SELECT url FROM Recipe WHERE Id=?", (rcp,))
		url = cur.fetchone()[0]
		urls.append(url)
	
	return (urls, int(max_match))

def get_blurbs(urls):
	blurbs = []
	for url in urls:
		(title, blurb) = get_summary(url)
		price = title[title.index('$'):]
		try:
			title = title.split(price)[0].split('Budget Bytes: ')[1]
		except:
			title = title.split(price)[0]
		blurb = blurb + "..."
		blurbs.append((title, price, blurb, url))
	return blurbs

	
def qualify_match(match):
	if match > 50:
		return ("the maximum match for these ingredients is %d per cent - pretty good!" % match)
	else:
		return ("the maximum match for these ingredients is %d per cent ...eh, could be better" % match)

if __name__ == '__main__':
    app.run()
